/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package virtui;

import java.net.*;
import java.io.*;

/**
 *
 * @author mark
 */
public class Client {
    
    Socket socket   = null;
    int port        = 0;  
    String hostname = null;
    
    
    public Client(String hostname, int port) {
        this.port = port;
        this.hostname = hostname;
    }
    
    
    
    public void sendMessage(String msg) {
        try {
            socket = new Socket(this.hostname, this.port );
            
            
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter    out   = new PrintWriter(socket.getOutputStream(), true);
            
            out.print(msg);
            out.close();
            
        } catch(Exception e) {
           System.out.print("Client: (sendMessage): Cannot connect to server.\n");
           
        }
        
    }
}
