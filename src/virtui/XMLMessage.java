/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package virtui;

/**
 *
 * @author mark
 */
public class XMLMessage {
    
    public String status, message, authtoken;
    
    public XMLMessage() {
        /* ? */
    }
    
    public void setStatus(String _status) {
        this.status = _status;
    }
    public void setMessage(String _message) {
        this.message = _message;
    }
    public void setAuthToken(String _authtoken) {
        this.authtoken = _authtoken;
    }
}
