/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package virtui;

import java.util.ArrayList;
import java.util.List;
import java.io.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;



public class XMLLoader {
    public XMLMessage parse(String _xml) throws XMLStreamException {
        
        Reader ss = new StringReader(_xml);
        String tagData = null;
                
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(ss);
        XMLMessage serverMessage = new XMLMessage();
        
        while(reader.hasNext()) {
            int event = reader.next();
            
            switch( event ) {
                
                case XMLStreamConstants.START_ELEMENT:
                    if("serverdata".equalsIgnoreCase(reader.getLocalName())) {
                        serverMessage = new XMLMessage();
                    }
                    break;
                    
                case XMLStreamConstants.CHARACTERS:
                    tagData = reader.getText().trim();
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    switch(reader.getLocalName())
                    {
                        case "status":
                            serverMessage.status  = tagData;
                            break;
                        case "message":
                            serverMessage.message = tagData;
                            break;
                        case "authtoken":
                            serverMessage.authtoken = tagData;
                            break;
                    }
                    break;
            }
        }
        return serverMessage;
    }
}
